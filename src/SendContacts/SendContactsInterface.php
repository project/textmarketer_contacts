<?php

/**
 * @file
 * Allows the site to send and receive user contacts to and from Text Marketer.
 */

namespace Drupal\textmarketer_contacts\SendContacts;

use Drupal\Core\Config;

/**
 * Provides an interface defining a client request type.
 */
interface SendContactsInterface {

}
